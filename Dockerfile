FROM eclipse-temurin:17-jre-alpine
ARG USER_ID=666
ARG USER_NAME=qaload

RUN adduser \
    --disabled-password \
    --home /home/$USER_NAME \
    --shell /bin/ash \
    --uid $USER_ID $USER_NAME

USER $USER_NAME
RUN mkdir /home/$USER_NAME/project
WORKDIR /home/$USER_NAME/project
ENTRYPOINT ["java", "-jar"]