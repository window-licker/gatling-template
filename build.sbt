import Dependencies._
import sbt.Keys.libraryDependencies
import sbtassembly.AssemblyKeys.assembly

enablePlugins(GatlingPlugin, AssemblyPlugin)

resolvers ++= Seq(
  "confluent" at "https://packages.confluent.io/maven",
)

libraryDependencies ++= gatling
libraryDependencies ++= logbackSetup
libraryDependencies ++= paramConfig
libraryDependencies ++= apacheCommons
libraryDependencies ++= kafkaSetup
libraryDependencies ++= clickSetup

lazy val root = (project in file("."))
    .settings(
        inThisBuild(List(
            version := "0.0.1-SNAPSHOT",
            scalaVersion := "2.13.10"
        )),
        name := "load-test",
        scalacOptions := Seq(
          "-encoding",
          "UTF-8",
          "-target:jvm-1.8",
          "-feature",
          "-unchecked",
          "-language:implicitConversions",
          "-language:postfixOps"
        ),
        assembly / mainClass := Some("io.gatling.app.Gatling"),
        assembly / assemblyJarName := s"${name.value}-${version.value}.jar",
        assembly / fullClasspath := (assembly / fullClasspath).value ++ (Test / fullClasspath).value,
        assembly / assemblyOutputPath := file(s"./${(assembly / assemblyJarName).value}"),
        ThisBuild / assemblyMergeStrategy := {
            case x if Assembly.isConfigFile(x) =>
                MergeStrategy.concat
            case PathList(ps@_*) if Assembly.isReadme(ps.last) || Assembly.isLicenseFile(ps.last) =>
                MergeStrategy.rename
            case PathList("META-INF", xs@_*) =>
                xs map {
                    _.toLowerCase
                } match {
                    case "manifest.mf" :: Nil | "index.list" :: Nil | "dependencies" :: Nil =>
                        MergeStrategy.discard
                    case ps@x :: xs if ps.last.endsWith(".sf") || ps.last.endsWith(".dsa") =>
                        MergeStrategy.discard
                    case "plexus" :: xs =>
                        MergeStrategy.discard
                    case "services" :: xs =>
                        MergeStrategy.filterDistinctLines
                    case "spring.schemas" :: Nil | "spring.handlers" :: Nil =>
                        MergeStrategy.filterDistinctLines
                    case _ => MergeStrategy.first
                }
            case PathList(ps@_*) if ps.last endsWith ".html" => MergeStrategy.last
            case "application.conf" => MergeStrategy.discard
            case "unwanted.txt" => MergeStrategy.discard
            case "gatling.conf" => MergeStrategy.concat
            case "gatling-akka-defaults.conf" => MergeStrategy.concat
            case "gatling-defaults.conf" => MergeStrategy.concat
            case "reference.conf" => MergeStrategy.concat
            case _ => MergeStrategy.first
        }
    )