import sbt.*

object Dependencies {
  lazy val gatling: Seq[ModuleID] = Seq(
    "io.gatling.highcharts" % "gatling-charts-highcharts",
    "io.gatling"            % "gatling-test-framework"
  ).map(_ % "3.9.3" % Test)

  lazy val apacheCommons: Seq[ModuleID] = Seq("org.apache.commons" % "commons-text" % "1.9")

  lazy val logbackSetup: Seq[ModuleID] = Seq(
    "ch.qos.logback" % "logback-core",
    "ch.qos.logback" % "logback-classic"
  ).map(_ % "1.2.10" % Test) ++ Seq(
    "org.codehaus.janino" % "janino" % "3.1.2"
  ) ++ Seq(
    "net.logstash.logback" % "logstash-logback-encoder" % "7.0.1"
  )

  lazy val kafkaSetup: Seq[ModuleID] = Seq(
    "ru.tinkoff" %% "gatling-kafka-plugin" % "0.11.3"
  ) ++ Seq(
    "com.sksamuel.avro4s" %% "avro4s-core" % "4.1.0"
  ) ++ Seq(
    "org.apache.avro" % "avro-compiler" % "1.11.1"
  ) ++ Seq(
    "org.apache.avro" % "avro" % "1.11.1"
  ) ++ Seq(
    "io.confluent" % "kafka-avro-serializer" % "7.3.0",
    "io.confluent" % "kafka-streams-avro-serde" % "7.3.0" exclude("org.apache.kafka", "kafka-streams-scala")
  )

  lazy val paramConfig: Seq[ModuleID] = Seq(
    "org.aeonbits.owner" % "owner" % "1.0.12"
  )

  lazy val clickSetup: Seq[ModuleID] = Seq(
    "ru.yandex.clickhouse"  % "clickhouse-jdbc" % "0.3.2"
  )

}