package helpers

import org.apache.avro.generic.{GenericData, GenericDatumWriter, GenericRecord}
import org.apache.avro.specific.SpecificDatumWriter
import org.apache.avro.file.DataFileWriter
import org.apache.avro.io.EncoderFactory
import org.apache.avro.Schema

import java.io.ByteArrayOutputStream

object AvroWriter {

    private def classToRecord[T <: Product](instance: T, schema: Schema): GenericData.Record = {
        val argumentNamesAndTheirValues = (instance.productElementNames zip instance.productIterator).toMap
        val record = new GenericData.Record(schema)
        for ((name, value) <- argumentNamesAndTheirValues) record.put(name, value)
        record
    }

    def classToFile[T <: Product](filename: String, instance: T, schema: Schema): Unit = {
        val record = classToRecord(instance, schema)
        val writer = new SpecificDatumWriter[GenericRecord](schema)
        val fileWriter = new DataFileWriter(writer)
        fileWriter.create(schema, new java.io.File(filename))
        try fileWriter.append(record) finally fileWriter.close()
    }

    def classToBytes[T <: Product](instance: T, schema: Schema): Array[Byte] = {
        val writer = new SpecificDatumWriter[GenericRecord](schema)
        val fileWriter = new DataFileWriter(writer)

        val out = new ByteArrayOutputStream()
        fileWriter.create(schema, out)

        val record = classToRecord(instance, schema)
        try fileWriter.append(record) finally fileWriter.close()

        out.close()
        out.toByteArray
    }

}
