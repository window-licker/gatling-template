package helpers

import io.gatling.app.Gatling
import io.gatling.core.config.GatlingPropertiesBuilder
import project.Simulation

object Debugger {

  def main(args: Array[String]): Unit = {

    val simulationClass = classOf[Simulation].getName

    val props = new GatlingPropertiesBuilder
    props.simulationClass(simulationClass)

    Gatling.fromMap(props.build)
  }
}
