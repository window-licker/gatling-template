package helpers

import org.aeonbits.owner.Config._
import org.aeonbits.owner.Config

@LoadPolicy(LoadType.MERGE)
@Sources (
    Array(
        "system:env",
        "system:properties",
        "classpath:simulation.properties"
    )
)
trait TestConfig extends Config {
    @Key("host.name")
    @DefaultValue("")
    def hostname(): String

    @Key("host.port")
    @DefaultValue("")
    def hostPort(): String

    @Key("url.scheme")
    @DefaultValue("")
    def scheme(): String

    @Key("proxy.host")
    @DefaultValue("127.0.0.1")
    def httpProxyHost(): String

    @Key("proxy.port")
    @DefaultValue("8080")
    def httpProxyPort(): Int

    @Key("clickhouse.host")
    @DefaultValue("localhost:8123")
    def clickhouseHost(): String

    @Key("clickhouse.table")
    @DefaultValue("default")
    def clickhouseTable(): String

    @Key("sim.loadmodel")
    @DefaultValue("opened")
    def loadModel(): String

    @Key("sim.intensity.end")
    @DefaultValue("1")
    def intensityEnd(): Int

    @Key("sim.intensity.begin")
    @DefaultValue("1")
    def intensityBegin(): Int

    @Key("sim.ramp.duration")
    @DefaultValue("1")
    def rampDuration(): Int

    @Key("sim.stage.duration")
    @DefaultValue("1")
    def stageDuration(): Int

    @Key("sim.stages")
    @DefaultValue("10")
    def stages(): Int

    @Key("sim.type")
    @DefaultValue("debug")
    def simType(): String
}
