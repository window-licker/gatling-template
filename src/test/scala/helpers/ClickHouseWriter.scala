package helpers

import project.ConfigManager

import java.sql._
import ru.yandex.clickhouse._
import ru.yandex.clickhouse.settings.ClickHouseProperties

import java.util.concurrent.{ConcurrentLinkedQueue, ExecutorService, Executors}
import scala.concurrent.duration.{Duration, SECONDS}
import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future}

object ClickHouseWriter {
  def apply(sqlTemplate: String): ClickHouseWriter = new ClickHouseWriter(sqlTemplate)
}

class ClickHouseWriter(val sqlTemplate: String) {

  val DB_URL: String = s"jdbc:clickhouse://${ConfigManager.clickhouseHost}/${ConfigManager.clickhouseTable}"
  val properties = new ClickHouseProperties
  val dataSource = new ClickHouseDataSource(DB_URL, properties)
  var conn: ClickHouseConnection = dataSource.getConnection

  val preparedStatement: PreparedStatement = conn.prepareStatement(sqlTemplate)

  val BUFFER: Int = 1000
  var batchSize: Int = 0

  var queue = new ConcurrentLinkedQueue[Map[String, String]]()

  def insertValues(values: Map[String, String]): Unit = synchronized {
    addToQ(values)
  }

  private def addToQ(seq: Map[String, String]): Unit = {
    queue.add(seq)
  }

  // Контекст нужен для вычисления функции в фоне
  // Можно заменить на дефолтный контекст import scala.concurrent.ExecutionContext.Implicits.global
  // Тогда ниже будет достаточно оставить только Future
  private val singleThread: ExecutorService = Executors.newSingleThreadExecutor()
  implicit val threadExeCtx: ExecutionContextExecutor = ExecutionContext.fromExecutor(singleThread)
  Future {
    val duration = Duration(1, SECONDS)
    while(true) {
      try {
          addToBatch()
      } catch {
          case e: SQLException => println(e.getMessage)
      }
      Thread.sleep(duration.toMillis)
    }
  }

  private def addToBatch(): Unit = {
    while (!queue.isEmpty) {
      val valuesToInsert: Map[String, String] = queue.poll()
      createSql(valuesToInsert)

      preparedStatement.addBatch()

      if ((batchSize + 1) % BUFFER == 0)  {
        preparedStatement.executeBatch()
      }

      batchSize += 1
    }
  }

  private def createSql(values: Map[String, String]): Unit = {
    preparedStatement.setString(1, values("requestName"))
    preparedStatement.setInt(2, values("timeResponse").toInt)

    val event = values("timestampInMillis").toLong
    preparedStatement.setTimestamp(3, new Timestamp(event))

    preparedStatement.setDate(4, new java.sql.Date(System.currentTimeMillis()))
  }


}