package project.cases

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import project.Feeders


object Scenarios {
    def apply(): ScenarioBuilder = new Scenarios().scnMain
}

class Scenarios {

    val scnMain: ScenarioBuilder = scenario("scn test")
        .feed(Feeders.feedUid)
        .exec(Actions.reqGet)

}
