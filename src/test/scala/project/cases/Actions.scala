package project.cases

import io.gatling.http.Predef._
import io.gatling.core.Predef._
import io.gatling.http.request.builder.HttpRequestBuilder
import project.ConfigManager

object Actions {

    val reqGet: HttpRequestBuilder = http("req test")
      .get(s"${ConfigManager.baseUrl}/test")

}