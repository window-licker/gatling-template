package project

import io.gatling.core.Predef._
import io.gatling.core.feeder.{BatchableFeederBuilder, FeederBuilderBase}

import java.util.UUID

object Feeders {
    val feedUid: Iterator[Map[String, String]] = Iterator.continually {
        Map("uid" -> UUID.randomUUID.toString)
    }

    val feedConfigId: BatchableFeederBuilder[String] = csv("data/configId.csv").circular

    val feedQuality: FeederBuilderBase[Int] = Array(Map("quality" -> 0), Map("quality" -> 1)).random
}
