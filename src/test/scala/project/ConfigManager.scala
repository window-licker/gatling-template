package project

import helpers.TestConfig
import org.aeonbits.owner.ConfigFactory

object ConfigManager {

  val cfg: TestConfig = ConfigFactory.create(classOf[TestConfig])

  val baseUrl: String = s"${cfg.scheme()}://${cfg.hostname()}:${cfg.hostPort()}"

  val proxyHost: String = cfg.httpProxyHost()

  val proxyPort: Int = cfg.httpProxyPort()

  val rampDuration: Int = cfg.rampDuration()

  val stageDuration: Int = cfg.stageDuration()

  val intensityEnd: Int = cfg.intensityEnd()

  val intensityBegin: Int = cfg.intensityBegin()

  val stages: Int = cfg.stages()

  val loadModel: String = cfg.loadModel()

  val simType: String = cfg.simType()

  val clickhouseHost: String = cfg.clickhouseHost()

  val clickhouseTable: String = cfg.clickhouseTable()

}
