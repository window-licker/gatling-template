package project

import io.gatling.core.Predef._
import io.gatling.core.protocol.Protocol
import io.gatling.core.structure.{PopulationBuilder, ScenarioBuilder}
import ConfigManager._

trait Runner extends Simulation {

  def makeDebugTest(pair: (ScenarioBuilder, Protocol)): PopulationBuilder = {
    val (scn, prot) = pair
    val test: PopulationBuilder = scn.inject(atOnceUsers(1))
        .protocols(prot)

    test
  }

  def makeMaxTestOpenModel(pair: (ScenarioBuilder, Protocol)): PopulationBuilder = {
    val (scn, prot) = pair
    val test: PopulationBuilder = scn.inject(
      incrementUsersPerSec(intensityEnd / stages)
        .times(stages)
        .eachLevelLasting(stageDuration)
        .separatedByRampsLasting(rampDuration)
        .startingFrom(intensityBegin)
    ).protocols(prot)

    test
  }

  def makeMaxTestClosedModel(pair: (ScenarioBuilder, Protocol)): PopulationBuilder = {
    val (scn, prot) = pair
    val test: PopulationBuilder = scn.inject(
      incrementConcurrentUsers(intensityEnd / stages)
          .times(stages)
          .eachLevelLasting(stageDuration)
          .separatedByRampsLasting(rampDuration)
          .startingFrom(intensityBegin)
    ).protocols(prot)

    test
  }

    def makeStableTestOpenModel(pair: (ScenarioBuilder, Protocol)): PopulationBuilder = {
    val (scn, prot) = pair
    val test: PopulationBuilder = scn.inject(
      rampUsersPerSec(0) to ConfigManager.intensityEnd during ConfigManager.rampDuration,
      constantUsersPerSec(ConfigManager.intensityEnd) during ConfigManager.stageDuration
    ).protocols(prot)

    test
  }

  def makeStableTestClosedModel(pair: (ScenarioBuilder, Protocol)): PopulationBuilder = {
    val (scn, prot) = pair
    val test: PopulationBuilder = scn.inject(
      rampConcurrentUsers(0) to ConfigManager.intensityEnd during ConfigManager.rampDuration,
      constantConcurrentUsers(ConfigManager.intensityEnd) during ConfigManager.stageDuration
    ).protocols(prot)

    test
  }

  def makeStableTestHelper(pair: (ScenarioBuilder, Protocol)): PopulationBuilder = {
    loadModel match {
      case "open" => makeStableTestOpenModel(pair)
      case "closed" => makeStableTestClosedModel(pair)
    }
  }

  def makeMaxTestHelper(pair: (ScenarioBuilder, Protocol)): PopulationBuilder = {
    loadModel match {
      case "open" => makeMaxTestOpenModel(pair)
      case "closed" => makeMaxTestClosedModel(pair)
    }
  }

  def load(contexts: List[(ScenarioBuilder, Protocol)]): SetUp = {
    simType match {
      case "debug" =>
        val profile: List[PopulationBuilder] = for (context <- contexts) yield makeDebugTest(context)
        setUp(profile)
      case "max" =>
        val profile: List[PopulationBuilder] = for (context <- contexts) yield makeMaxTestHelper(context)
        setUp(profile)
      case "stable" =>
        val profile: List[PopulationBuilder] = for (context <- contexts) yield makeStableTestHelper(context)
        setUp(profile)
    }
  }

}
