package project

import io.gatling.core.protocol.Protocol
import io.gatling.core.structure.ScenarioBuilder
import project.cases.Scenarios

class Simulation extends Runner {

    val profile = List[(ScenarioBuilder, Protocol)] (
        (Scenarios(), ProtocolConfig.httpProtocol)
    )

    load(profile)
}
