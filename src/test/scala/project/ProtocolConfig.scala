package project

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder
import org.aeonbits.owner.ConfigFactory
import helpers.TestConfig

object ProtocolConfig {

    val httpProtocol: HttpProtocolBuilder = http
        .baseUrl(s"${ConfigManager.baseUrl}")
        .acceptHeader("application/json")
        .userAgentHeader("Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:97.0) Gecko/20100101 Firefox/97.0")
        .acceptEncodingHeader("gzip, deflate, br")
        .acceptLanguageHeader("en-US,en;q=0.5")

    val httpProtocolProxy: HttpProtocolBuilder = httpProtocol.proxy(
        Proxy(ConfigManager.proxyHost,ConfigManager.proxyPort)
    )
}
