#!/bin/bash
PATCH_VERSION=${1:-1}
IMAGE_NAME=${2:-qaload/gatling}
SIMULATION_CLASS="${3:-simulation}"
IMAGE_VERSION="0.0.$PATCH_VERSION"
IMAGE="$IMAGE_NAME:$IMAGE_VERSION"

if [[  -z $(docker images | grep "$IMAGE_NAME") ]]
then
  docker build -t "$IMAGE" .
fi

test_class_name=$(jar tf ./*.jar | \
                    grep -Ei "project/*/${SIMULATION_CLASS}" | \
                    sed 's/\//./g' | \
                    sed 's/.class//g')

docker run \
        --rm -it \
        --network host \
        -v "$(pwd):/home/qaload/project" \
        "$IMAGE" \
        *.jar -s "$test_class_name"
